.. index::
   pair: Nextcloud; Universal access

.. _nextcloud_universal_access:

====================================
Nextcloud Universal access
====================================

- https://docs.nextcloud.com/server/latest/user_manual/en/universal_access.html
- https://mastodon.xyz/@nextcloud/111688486208092537

.. tags:: Nextcloud

Universal access is very important to us. 

We follow web standards and check to make everything usable also with 
keyboard and assistive software such as screen readers. 

We aim to be compliant with the Web Content Accessibility Guidelines 2.1 
on AA level, with the high contrast theme even on AAA level. 

We also follow the German BITV 2.0 guidelines.

If you find any issues, don’t hesitate to report them on our issue tracker. 
And if you want to get involved, come join our design team!
