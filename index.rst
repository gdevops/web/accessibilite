
.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/web/accessibilite/rss.xml>`_

.. _a11y:

====================================
Tuto Web Accessibility (a11y)
====================================


.. toctree::
   :maxdepth: 6

   tutos/tutos

